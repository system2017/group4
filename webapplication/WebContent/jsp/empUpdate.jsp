<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
</head>
<body>
	<h1>社員更新</h1>
	<form action="/webapplication/empUpdateConfirmButton" method="post">
	${errormsg }
		<table border="1">
			<tr>
				<th>社員ID</th>
				<td><c:out value="${empUpdate.empId}"></c:out></td>
			</tr>
			<tr>
				<th>社員名(必須項目)</th>
				<td><input required  type="text" name="emp_name" value="${empUpdate.empName }"></td>
			</tr>
			<tr>
				<th>メールアドレス</th>
				<td><input type="email" name="mail" value="${empUpdate.empMail }"></td>
			</tr>
			<tr>
				<th>生年月日<br>(yyyy(西暦)-mm(月)-dd(日))</th>
				<td><input type="date" name="birthday" value="${empUpdate.empBirthday }"></td>
			</tr>
			<tr>
				<th>給与(半角数字)</th>
				<td><input type="number" name="salary" value="${empUpdate.empSalary }"></td>
			</tr>
			<tr>
				<th>前パスワード</th>
				<td><input type="password" name="passwordPrev"></td>
			</tr>
			<tr>
				<th>新パスワード</th>
				<td><input type="password" name="password"></td>
			</tr>
			<tr>
				<th>新パスワード（確認）</th>
				<td><input type="password" name="passwordConfirm"></td>
			</tr>
			<tr>
			<th>部署</th>
			<td><input type="dpid" name="empdepartment" value="${empUpdate.empDepId }"></td>
			</tr>
		</table>
		<p>
			<input type="submit" value="確認">
		</p>
	</form>
</body>
</html>