<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>社員更新</h1>
	<h2>以下の内容を更新してよろしいでしょうか？</h2>
	<table border="1">
		<tr>
			<th>社員ID</th>
			<td>${emp2.empId}</td>
		</tr>
		<tr>
			<th>社員名</th>
			<td>${emp2.empName}</td>
		</tr>
		<tr>
			<th>メールアドレス</th>
			<td>${emp2.empMail }</td>
		</tr>
		<tr>
			<th>生年月日</th>
			<td>${emp2.empBirthday}</td>
		</tr>
		<tr>
			<th>給与</th>
			<td>${emp2.empSalary}</td>

		</tr>
		<tr>
			<th>部署</th>
			<td>${emp2.empDepId}</td>
		</tr>
	</table>
	<form action="/webapplication/empUpdateCompleteButton" method="post">
		<p>
			<input type="submit" value="確定">
		</p>
	</form>

</body>
</html>