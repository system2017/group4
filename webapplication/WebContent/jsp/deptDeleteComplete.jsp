<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="css/main.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>部署管理システム</title>
</head>
<body>
	<div>
		<font size=7>部署削除</font><br> <font size=5>以下の部署を消しました。</font><br>
	</div>
	<table border=1>
		<tr>
			<td><div>部署名ID:</div></td>
			<td><div>${deptUpdate.deptId}</div></td>
		</tr>
		<tr>
			<td><div>部署名:</div></td>
			<td><div>${deptUpdate.deptName}</div></td>
		</tr>

	</table>
	<form action="/webapplication/complete_to_menu">
		<input type="submit" value="メニューへ">
	</form>
</body>
</html>