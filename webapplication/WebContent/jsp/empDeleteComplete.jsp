﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="css/main.css" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>社員削除</h1>
	<h2>以下の内容を削除しました</h2>
	<table border="1">
		<tr>
			<th>社員ID</th>
			<td>${empUpdate.empId}</td>
		</tr>
		<tr>
			<th>社員名</th>
			<td>${empUpdate.empName}</td>
		</tr>
		<tr>
			<th>メールアドレス</th>
			<td>${empUpdate.empMail}</td>
		</tr>
		<tr>
			<th>生年月日</th>
			<td>${empUpdate.empBirthday}</td>
		</tr>
		<tr>
			<th>給与</th>
			<td>${empUpdate.empSalary}</td>
		</tr>
		<tr>
			<th>部署</th>
			<td>${empUpdate.empDepId}</td>
		</tr>
	</table>
	<a href="/webapplication/menuview">メニューへ</a>
</body>
</html>