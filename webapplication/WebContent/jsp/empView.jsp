<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>社員一覧</h1>
	<table border="1">
		<tr>
			<th>社員ID</th>
			<th>社員名</th>
			<th>メールアドレス</th>
			<th>生年月日</th>
			<th>部署</th>
			<th></th>
		</tr>

		<c:forEach items="${empList}" var="e">
			<tr>
				<td>${e.empId}</td>
				<td>${e.empName}</td>
				<td>${e.empMail}</td>
				<td>${e.empBirthday}</td>
				<td>${e.empDepId}</td>
				<td>


					<form action="/webapplication/empdetailbutton" method="post">

						<p>
							<input type="submit" value="詳細へ"> <input type="hidden"
								value="${e.empId}" name="EMPID">
						</p>


					</form>

				</td>
			</tr>
		</c:forEach>


	</table>


</body>
</html>