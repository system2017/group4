package dto;

import java.sql.Date;

public class Emp {
	private String empId;
	private String empName;
	private String empMail;
	private Date empBirthday;
	private int empSalary;
	private String empPassword;
	private String empDepId;
	private String empDepName;

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpMail() {
		return empMail;
	}

	public void setEmpMail(String empMail) {
		this.empMail = empMail;
	}

	public Date getEmpBirthday() {
		return empBirthday;
	}

	public void setEmpBirthday(Date empBirthday) {
		this.empBirthday = empBirthday;
	}

	public int getEmpSalary() {
		return empSalary;
	}

	public void setEmpSalary(int empSalary) {
		this.empSalary = empSalary;
	}

	public String getEmpPassword() {
		return empPassword;
	}

	public void setEmpPassword(String empPassword) {
		this.empPassword = empPassword;
	}

	public String getEmpDepId() {
		return empDepId;
	}

	public void setEmpDepId(String empDepId) {
		this.empDepId = empDepId;
	}

	public String getEmpDepName() {
		return empDepName;
	}

	public void setEmpDepName(String empDepName) {
		this.empDepName = empDepName;
	}

}
