package dto;

public class Dep {
	private String depId;
	private String depName;

	public String getEmpId() {
		return depId;
	}

	public void setEmpId(String empId) {
		this.depId = empId;
	}

	public String getEmpName() {
		return depName;
	}

	public void setEmpName(String empName) {
		this.depName = empName;
	}

	public String getDepId() {
		return depId;
	}

	public void setDepId(String depId) {
		this.depId = depId;
	}

	public String getDepName() {
		return depName;
	}

	public void setDepName(String depName) {
		this.depName = depName;
	}

}
