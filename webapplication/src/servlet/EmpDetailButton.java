package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.Emp;

/**
 * Servlet implementation class EmpDetailButton
 */
@WebServlet("/empdetailbutton")
public class EmpDetailButton extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String empid = request.getParameter("EMPID");
		EmpDAO empdao = new EmpDAO();
		Emp empdto = empdao.getOne(empid);
		if(empdto != null){
			HttpSession session = request.getSession(false);
			session.setAttribute("empUpdate", empdto);

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/empDetail.jsp");
			requestDispatcher.forward(request, response);

		}else{
//			RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/error.jsp");
//			requestDispatcher.forward(request, response);

		}



	}


}
