package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepDAO;
import dto.Dep;

/**
 * Servlet implementation class DepDeleteCompleteButton
 */
@WebServlet(name = "depdeletecompletebutton", urlPatterns = { "/depdeletecompletebutton" })
public class DepDeleteCompleteButton extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		Dep depUpdate = (Dep) session.getAttribute("depUpdate");
		DepDAO depDAO = new DepDAO();
		depDAO.delete(depUpdate.getDepId());
		session.removeAttribute("depUpdate");
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("jsp/depDeleteComplete.jsp");
		requestDispatcher.forward(request, response);

	}

}
