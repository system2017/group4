package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.Emp;

/**
 * Servlet implementation class EmpRegisterCompleteButton
 */
@WebServlet("/empregistercompletebutton")
public class EmpRegisterCompleteButton extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// String jspPath = "";
		HttpSession session = request.getSession(false);
		Emp emp = (Emp) session.getAttribute("emp");

		EmpDAO empDAO = new EmpDAO();
		empDAO.insert(emp);

		request.getRequestDispatcher("/jsp/empComplete.jsp").forward(request, response);


	}
}
