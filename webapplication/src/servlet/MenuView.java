package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.Emp;

/**
 * Servlet implementation class MnueView
 */
@WebServlet(name = "menuview", urlPatterns = { "/menuview" })
public class MenuView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession(false);
		Emp emp = (Emp) session.getAttribute("empLogin");

		request.setAttribute("empName", emp.getEmpName());

		RequestDispatcher requestDispathcer = request.getRequestDispatcher("jsp/menu.jsp");
		requestDispathcer.forward(request, response);

	}

}
