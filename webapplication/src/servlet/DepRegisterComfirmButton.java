package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DepDAO;
import dto.Dep;

/**
 * Servlet implementation class DepRegisterComfirmButton
 */
@WebServlet("/depregistercomfirmbutton")
public class DepRegisterComfirmButton extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		Dep dep = new Dep();

		String depId = request.getParameter("DEPID");
		String depName = request.getParameter("DEPNAME");

		DepDAO depDAO = new DepDAO();
		Dep dao = depDAO.getOne(depId);

		if (dao.equals(null)) {
			dep.setDepId(depId);
			dep.setDepName(depName);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/depRegisterConfirm.jsp");
			requestDispatcher.forward(request, response);
		} else {
			response.sendRedirect("mnueview");

		}
	}

}
