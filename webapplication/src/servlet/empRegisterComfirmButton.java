package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.Emp;

/**
 * Servlet implementation class empRegisterComfirmButton
 */
@WebServlet("/empregistercomfirmbutton")
public class empRegisterComfirmButton extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String empId;
		String empName;
		String empMail;
		Date empBirthday;
		int empSalary;
		String empPassword;
		//String empPasswordComfirm = request.getParameter("EMPPASSWORDCOMFIRM");
		String empDepId;


		empId = request.getParameter("EMPID");
		empName = request.getParameter("EMPNAME");
		empMail = request.getParameter("EMPMAIL");
		empBirthday = Date.valueOf(request.getParameter("EMPBIRTHDAY"));
		empSalary = Integer.parseInt(request.getParameter("EMPSALARY"));
		empPassword = request.getParameter("EMPPASSWORD");
		empDepId = request.getParameter("EMPDEPID");


		Emp emp = new Emp();

		session.setAttribute("emp", emp);

		emp.setEmpId(empId);
		emp.setEmpName(empName);
		emp.setEmpMail(empMail);
		emp.setEmpBirthday(empBirthday);
		emp.setEmpPassword(empPassword);
		emp.setEmpSalary(empSalary);
		emp.setEmpDepId(empDepId);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empComfirm.jsp");
		requestDispatcher.forward(request, response);
//		if (empPassword.equals("empPasswordComfirm")) {
//			emp.setEmpPassword(empPassword);
//			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empConfirm.jsp");
//			requestDispatcher.forward(request, response);
//			return;
//		}
//
//		if (empId.equals("")) {
//			request.setAttribute("message", "Name not existed");
//			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empRegister.jsp");
//			requestDispatcher.forward(request, response);
//			return;
//		}
//
//		if (empPassword.equals("empPasswordComfirm")) {
//			request.setAttribute("message", "Input password failed");
//			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empRegister.jsp");
//			requestDispatcher.forward(request, response);
//			return;
//		}

	}

}
