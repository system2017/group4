package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.Emp;

/**
 * Servlet implementation class empUpdateCompleteButton
 */
@WebServlet("/empUpdateCompleteButton")
public class empUpdateCompleteButton extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Emp empUpdate = (Emp) session.getAttribute("emp2");
		EmpDAO empDAO = new EmpDAO();
		empDAO.update(empUpdate);
//		session.removeAttribute("emp2");

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empUpdateComplete.jsp");
		requestDispatcher.forward(request, response);
	}

}
