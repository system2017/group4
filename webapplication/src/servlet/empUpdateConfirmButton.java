package servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.Emp;

/**
 * Servlet implementation class empUpdateConfirmButton
 */
@WebServlet("/empUpdateConfirmButton")
public class empUpdateConfirmButton extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		Emp empupdate = (Emp)session.getAttribute("empUpdate");
		String empName = request.getParameter("emp_name");
		String empMail = request.getParameter("mail");
		Date empBirthday = Date.valueOf(request.getParameter("birthday"));
		int empSalary = Integer.parseInt(request.getParameter("salary"));
		String empPasswordPrev = request.getParameter("passwordPrev");
		String empPassword = request.getParameter("password");
		String empPasswordComfirm = request.getParameter("passwordConfirm");
		String empDepId = request.getParameter("empdepartment");

		Emp emp = new Emp();

		session.setAttribute("emp2", emp);

		emp.setEmpId(empupdate.getEmpDepId());
		emp.setEmpName(empName);
		emp.setEmpMail(empMail);
		emp.setEmpBirthday(empBirthday);
		emp.setEmpSalary(empSalary);
		emp.setEmpId(empDepId);

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empUpdateConfirm.jsp");
		requestDispatcher.forward(request, response);



	/*	if (empPassword.equals("empPasswordComfirm")) {
			emp.setEmpPassword(empPassword);

			return;
		}



		if (empPassword.equals("empPasswordComfirm")) {
			request.setAttribute("message", "Input password failed");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empRegister.jsp");
			requestDispatcher.forward(request, response);
			return;
		}
		emp.setEmpId(empId);
		emp.setEmpName(empName);
		emp.setEmpMail(empMail);
		emp.setEmpBirthday(empBirthday);
		emp.setEmpSalary(empSalary);
		emp.setEmpId(empId);
		if (empPassword.equals("empPasswordComfirm")) {
			request.setAttribute("errornotmatch", "Password error");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empRegister.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		if (empId.equals("")) {
			request.setAttribute("error", "Name not existed");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empRegister.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		if (empPassword.equals("empPasswordComfirm")) {
			request.setAttribute("errornotmatch", "Password error");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empRegister.jsp");
			requestDispatcher.forward(request, response);
			return;
		}




		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/empUpdateConfirm.jsp");
		requestDispatcher.forward(request, response);
	} */

}
}


