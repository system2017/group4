package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.Emp;

/**
 * Servlet implementation class LoginCheck
 */
@WebServlet(name = "logincheck", urlPatterns = { "/logincheck" })
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String empid = request.getParameter("EMPID");
		String password = request.getParameter("PASSWORD");

		EmpDAO empDAO = new EmpDAO();
		Emp emp = empDAO.getOne(empid); // empはempDTO(一人分のデータ全体)

		if (emp == null) {

			response.sendRedirect("loginview");

		} else {

			if (password.equals(emp.getEmpPassword())) {

				HttpSession session = request.getSession(true);
				session.setAttribute("empLogin", emp);

				request.setAttribute("empName", emp.getEmpName());

				RequestDispatcher requestDispathcer = request.getRequestDispatcher("jsp/menu.jsp");
				requestDispathcer.forward(request, response);

			} else {

				request.setAttribute("errorMessage", "パスワードが違います");

				RequestDispatcher requestDispathcer = request.getRequestDispatcher("jsp/login.jsp");
				requestDispathcer.forward(request, response);

			}
		}
	}
}
