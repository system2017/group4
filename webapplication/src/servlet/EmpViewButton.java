package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EmpDAO;
import dto.Emp;

/**
 * Servlet implementation class EmpViewButton
 */
@WebServlet(name = "empviewbutton", urlPatterns = { "/empviewbutton" })
public class EmpViewButton extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		EmpDAO empDAO = new EmpDAO();
		ArrayList<Emp> empList = empDAO.getAll();

		request.setAttribute("empList", empList);

		RequestDispatcher requestDispathcer = request.getRequestDispatcher("jsp/empView.jsp");
		requestDispathcer.forward(request, response);
	}

}
