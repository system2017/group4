package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.Dep;
import util.MyConnectionManager;

public class DepDAO {
	public ArrayList<Dep> getAll() {
		MyConnectionManager myconnectionManager = new MyConnectionManager();

		Connection connection = myconnectionManager.getConnection();

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        dep");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<Dep> list = new ArrayList<>();

			while (rs.next()) {
				String c1 = rs.getString("depid");
				String c2 = rs.getString("depname");

				Dep dep = new Dep();
				dep.setDepId(c1);
				dep.setDepName(c2);

				list.add(dep);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			myconnectionManager.closeConnection();
		}
	}

	public Dep getOne(String depid) {
		MyConnectionManager connectionManager = new MyConnectionManager();

		Connection connection = connectionManager.getConnection();

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        dep");
			sb.append("        WHERE");
			sb.append("        depid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			prepareStatement.setString(1, depid);

			ResultSet rs = prepareStatement.executeQuery();

			Dep dep = null;
			while (rs.next()) {
				String c1 = rs.getString("depid");
				String c2 = rs.getString("depname");

				dep.setDepId(c1);
				dep.setDepName(c2);

			}
			return dep;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}

}
