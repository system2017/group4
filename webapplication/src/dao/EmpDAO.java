package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dto.Emp;
import util.MyConnectionManager;

public class EmpDAO {
	public ArrayList<Emp> getAll() {
		MyConnectionManager myconnectionManager = new MyConnectionManager();

		Connection connection = myconnectionManager.getConnection();

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        emp");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			ResultSet rs = prepareStatement.executeQuery();

			ArrayList<Emp> list = new ArrayList<>();

			while (rs.next()) {
				String c1 = rs.getString("empid");
				String c2 = rs.getString("empname");
				String c3 = rs.getString("mail");
				Date c4 = rs.getDate("birthday");
				int c5 = rs.getInt("salary");
				String c6 = rs.getString("password");
				String c7 = rs.getString("deptid");

				Emp emp = new Emp();
				emp.setEmpId(c1);
				emp.setEmpName(c2);
				emp.setEmpMail(c3);
				emp.setEmpBirthday(c4);
				emp.setEmpSalary(c5);
				emp.setEmpPassword(c6);
				emp.setEmpDepId(c7);

				list.add(emp);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			myconnectionManager.closeConnection();
		}
	}

	public Emp getOne(String empid) {
		MyConnectionManager connectionManager = new MyConnectionManager();

		Connection connection = connectionManager.getConnection();

		try {
			StringBuffer sb = new StringBuffer();
			sb.append("SELECT");
			sb.append("        *");
			sb.append("    FROM");
			sb.append("        emp");
			sb.append("        WHERE");
			sb.append("        empid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());

			prepareStatement.setString(1, empid);

			ResultSet rs = prepareStatement.executeQuery();

			Emp emp = null;
			while (rs.next()) {
				String c1 = rs.getString("empid");
				String c2 = rs.getString("empname");
				String c3 = rs.getString("mail");
				Date c4 = rs.getDate("birthday");
				int c5 = rs.getInt("salary");
				String c6 = rs.getString("password");
				String c7 = rs.getString("deptid");

				emp = new Emp(); // 訂正箇所
				emp.setEmpId(c1);
				emp.setEmpName(c2);
				emp.setEmpMail(c3);
				emp.setEmpBirthday(c4);
				emp.setEmpSalary(c5);
				emp.setEmpPassword(c6);
				emp.setEmpDepId(c7);

			}
			return emp;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			connectionManager.closeConnection();
		}
	}

	public void delete(String empid) {
		MyConnectionManager myconnectionManager = new MyConnectionManager();

		try {
			Connection connection = myconnectionManager.getConnection();

			 StringBuffer sb = new StringBuffer();
			 sb.append("DELETE");
			 sb.append(" FROM");
			 sb.append(" emp");
			 sb.append(" WHERE");
			 sb.append(" empid = ?;");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, empid);
			int result = prepareStatement.executeUpdate();

			if (result > 0) {
				System.out.println("Delete Successful! ");
			} else {
				System.out.println("Delete Failed!  ");
			}
			myconnectionManager.commitTransaction();

		} catch (SQLException e) {
			myconnectionManager.rollbackTransaction();
		} finally {
			myconnectionManager.closeConnection();
		}
	}



	public void update(Emp emp) {
		MyConnectionManager myconnectionManager = new MyConnectionManager();

		try {
			Connection connection = myconnectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("UPDATE");
			sb.append("        emp");
			sb.append("    SET");
			sb.append("        empid = ?");
			sb.append("        empname = ?");
			sb.append("    WHERE");
			sb.append("        empid = ?");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, emp.getEmpId());
			prepareStatement.setString(2, emp.getEmpName());
			prepareStatement.setString(3, emp.getEmpId());

			int result = prepareStatement.executeUpdate();
			myconnectionManager.commitTransaction();

		} catch (SQLException e) {
			myconnectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			myconnectionManager.closeConnection();
		}

	}

	public void insert(Emp emp) {
		MyConnectionManager myconnectionManager = new MyConnectionManager();

		try {
			Connection connection = myconnectionManager.getConnection();

			StringBuffer sb = new StringBuffer();
			sb.append("INSERT");
			sb.append("    INTO");
			sb.append("        emp(");
			sb.append("            empid");
			sb.append("            ,empname");
			sb.append("                ,mail");
			sb.append("	                  ,birthday");
			sb.append("                      ,salary");
			sb.append("                         ,password");
			sb.append("                             ,deptid");
			sb.append("        )");
			sb.append("    VALUES");
			sb.append("        (");
			sb.append("             ?");
			sb.append("             ,?");
			sb.append("              ,?");
			sb.append("               ,?");
			sb.append("                ,?");
			sb.append("                 ,?");
			sb.append("                  ,?");
			sb.append("        );");

			PreparedStatement prepareStatement = connection.prepareStatement(sb.toString());
			prepareStatement.setString(1, emp.getEmpId());
			prepareStatement.setString(2, emp.getEmpName());
			prepareStatement.setString(3, emp.getEmpMail());
			prepareStatement.setDate(4, emp.getEmpBirthday());
			prepareStatement.setInt(5, emp.getEmpSalary());
			prepareStatement.setString(6, emp.getEmpPassword());
			prepareStatement.setString(7, emp.getEmpDepId());

			int result = prepareStatement.executeUpdate();

			myconnectionManager.commitTransaction();

			return;
		} catch (SQLException e) {
			myconnectionManager.rollbackTransaction();
			throw new RuntimeException(e);
		} finally {
			myconnectionManager.closeConnection();
		}

	}

}
